##Multi-tenancy node start-up app template

#Create followig folders if not available in nginx
-conf/sites-available
-conf/sites-enabled


#Steps 1: create conf for domain e.g. tenant1.domain.com inside sites-available

tenant1.conf 
```
#!javascript

# the IP(s) on which your node server is running. I chose port 3000.
upstream tenant1.vizob1.com {
    server 127.0.0.1:3000; //Node server
    keepalive 8;
}

# the nginx server instance
server {
    listen 0.0.0.0:80; //listen on
    server_name tenant1.vizob1.com;
    access_log /nginx/logs/tenat1_vizob1.log;

    # pass the request to the node.js server with the correct headers
    # and much more can be added, see nginx config options
    location / {
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_set_header X-NginX-Proxy true;

      proxy_pass http://tenant1.vizob1.com/;
      proxy_redirect off;
    }
 }
```

--------------------------------------------------------------
#Steps 2: create symlinks in conf/sites-enabled to these files with conf/sites-enabled

```
#!javascript

mklink /h "conf/sites-enabled/site-name.conf" "conf/sites-available/conf-file-you-just-created.conf"
```

This will create new config file inside site-enabled 

--------------------------------------------------------------
#Step 3: update conf/nginx.conf to used those conf files onlu update chtml session.

```
#!javascript

http {
    include       mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;
	include upstreams-enabled/*.conf;
	include sites-enabled/*.conf;
}
```

----------------------------------------------------
\Step 4:
Make sure node app run forever, used pm2, demon, etc.